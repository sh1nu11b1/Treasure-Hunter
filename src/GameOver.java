import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;


public class GameOver  //the window of Game Over
{
	 static JButton JB;
	 JFrame jf;
	 SetTrasure settrasure;
	 int x,y;

	JTextArea JT;
	public GameOver(int score,SetTrasure settrasure,int xx,int yy)// The construction of game technology and sent into the score of this time and settrasure(include mainframe)
	{
		// TODO Auto-generated constructor stub
		x=xx;
		y=yy;
		this.settrasure=settrasure;
		jf=new JFrame("Game over!");
		jf.setBounds(250, 350, 300, 200);
		JT=new JTextArea();
		String Score=String.valueOf(score);
		JT.setText("A Trap!  Score:"+Score);
		JB=new JButton("OK");

		JB.setBounds(100, 120, 100, 30);
		JB.addActionListener(new GameoverController(this, settrasure));  // add the listener, sent out itself and settrasure
		jf.add(JB);

		jf.add(JT);
		jf.setVisible(true);
	}

}
