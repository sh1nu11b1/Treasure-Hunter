import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import org.json.JSONException;


public class GameoverController implements ActionListener {
	GameOver gameover;
	SetTrasure settrasure;
	int x,y;
	
	public GameoverController(GameOver gameover,SetTrasure settrasure) {
		// TODO Auto-generated constructor stub
		this.gameover=gameover;
		this.settrasure=settrasure;
		x=settrasure.x;
		y=settrasure.y;
	}

	@SuppressWarnings("static-access")
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		// TODO Auto-generated method stub
		if(e.getSource()==gameover.JB)  //if clicked button "OK" of gameover
		{
			this.gameover.jf.setVisible(false);

			try {
				new Rank(settrasure,x,y);
			} catch (IOException | JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}  // create a window to display a ranking
		}
	}

}
