import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

import org.json.JSONException;

//import ExtraCredit.Instruction;
//import ExtraCredit.InvalidException;


public class MainFrame  {
    JFrame mainframe;
    String[][] rank;
    String name;
    int score;  //the score of this time
    String str;
    JTextArea JT; //the content of the beginning instruction
    JButton JB;//Start Button
    int x,y;
    static boolean gameover=true;
	public MainFrame(int xx,int yy) throws IOException, JSONException {
		// TODO Auto-generated constructor stub
		score=0;    //initialize the score of this time (=0)
		x=xx;
		y=yy;
		GetRank getrank=new GetRank();
		rank=getrank.Getrank();  //get the rank which stored
		if(rank[0][0]==null)rank[0][0]="0"; //if no rank,set rank[0][0] to represent no data
		mainframe=new JFrame("Treasure Hunter");
		mainframe.setBounds(200, 300, x*133, y*133); //set the size and position of the main window
		mainframe.setLayout(null);
		new Start(this,JB,JT);
	    JB.addActionListener(new MainFrameController(this));
	    mainframe.setVisible(true);
	}

	
	
	public static void main(String[] args) throws IOException, JSONException {
		int width,high;
		if(args.length<2)  {
			new MainFrame(3,3);
			System.out.println("A default gird will be created");

		//if user input something
		}
		else{
			width = Integer.parseInt(args[0]);
			high = Integer.parseInt(args[1]);
			new MainFrame(width,high);

			} 	

	}

}
