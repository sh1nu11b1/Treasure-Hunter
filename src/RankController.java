import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class RankController implements ActionListener{
	Rank rank;
	SetTrasure settrasure;
	int trap;
	int x,y;
	
	public RankController(Rank rank,SetTrasure settrasure) {
		// TODO Auto-generated constructor stub
		this.rank=rank;
		this.settrasure=settrasure;
		x=settrasure.x;
		y=settrasure.y;
	}

	@SuppressWarnings("static-access")
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		//if click the button"NO", quit the project
		if(e.getSource()==rank.nobutton){
			System.out.print("no");
			System.exit(0);
		}
		//if click the button"YES", initialize 9 blocks and start a new game
		if(e.getSource()==rank.JB){
			new Player(settrasure.mf,x,y);
			rank.jf.setVisible(false);
			
			trap=(int) ((int)(1+Math.random()*(9-1+1)));//generate random numbers 1-9
			System.out.println(trap);
			for(int i=0;i<x*y;i++)
			{
				if(i!=(trap-1))
				settrasure.tb[i].resert("Unknown",false);
				else
				settrasure.tb[i].resert("Unknown", true);
		
			}
			
		 settrasure.mf.gameover=false;
			
           settrasure.mf.score=0;
		}
	}

}
