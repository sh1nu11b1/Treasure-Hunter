import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

import org.json.JSONException;


public class Rank {
    static JButton JB;
    static JButton nobutton;
    JFrame jf;
    JTextArea JT;
    GameOver gameover;
    SetTrasure settrasure;
    int x,y;
	public Rank(SetTrasure settrasure,int xx,int yy) throws IOException, JSONException {
		x=xx;
		y=yy;
		// TODO Auto-generated constructor stub
		this.settrasure=settrasure;
		jf=new JFrame("Rank");
		// TODO Auto-generated constructor stub
		jf.setBounds(250, 350, 300, 200);
		JT=new JTextArea("");
		GetTime gettime=new GetTime();
		//whether the score of this time be added into rank
		int ranknum=Integer.parseInt(settrasure.mf.rank[0][0]);
		if(ranknum<5)
		{
		ranknum+=1;
		settrasure.mf.rank[0][0]=Integer.toHexString(ranknum);
	    settrasure.mf.rank[ranknum][1]=String.valueOf(settrasure.mf.score);
	    settrasure.mf.rank[ranknum][0]=settrasure.mf.name;
	    settrasure.mf.rank[ranknum][2]=gettime.Gettime();
	    for(int i=ranknum+1;i<6;i++)
	    	 settrasure.mf.rank[i][1]="0";
		}
		
		else if(settrasure.mf.score>Integer.parseInt(settrasure.mf.rank[5][1])){
			settrasure.mf.rank[5][1]=String.valueOf(settrasure.mf.score);
			 settrasure.mf.rank[5][0]=settrasure.mf.name;
			 settrasure.mf.rank[5][2]=gettime.Gettime();
			
		}
		
		 //Sorting rank array
		bubbleSort b=new bubbleSort();
		settrasure.mf.rank=b.BubbleSort(settrasure.mf.rank);
		
		// the content of "rank" text
		String str="  Name       score        time"+'\n';
		for(int i=1;i<=Integer.parseInt(settrasure.mf.rank[0][0]);i++){
    		str+=Integer.toHexString(i)+". "+settrasure.mf.rank[i][0]+":    "+(settrasure.mf.rank[i][1])+"        "+settrasure.mf.rank[i][2]+'\n';
    	}
    	str+="Try Again?";
    	JT.setText(str);
    	
    	//store the content of "rank"
         new SaveRank(settrasure.mf.rank);
    	
      // set two buttons, and add  listeners to them
    	nobutton=new JButton("NO");
    	nobutton.setBounds(150, 120, 100, 30);
    	JB=new JButton("YES");
		JB.setBounds(50, 120, 100, 30);
		JB.addActionListener(new RankController(this,this.settrasure));
		nobutton.addActionListener(new RankController(this,this.settrasure));
		jf.add(JB);
		jf.add(nobutton);
		jf.add(JT);
		jf.setVisible(true);
	}
	
	

}
