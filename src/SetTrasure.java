import java.awt.GridLayout;

import javax.swing.JPanel;




public class SetTrasure {
	MainFrame mf;
	Treasure[] tb;
	int x,y;
	@SuppressWarnings("static-access")
	public  SetTrasure(MainFrame mf,int xx,int yy) // Initialize 9 blocks and set the properties for each the block
	{
		// TODO Auto-generated constructor stub
		x=xx;
		y=yy;
		this.mf=mf;
		int trap=0;
		trap=(int)(1+Math.random()*(9-1+1));
//		mf.mainframe.setLayout(new GridLayout(x,y));
		tb=new Treasure[x*y];
		for(int i=0;i<x*y;i++)
		{
			if(i!=(trap-1))
			tb[i]=new Treasure(false);
			else
			tb[i]=new Treasure(true);
			tb[i].addActionListener(new TreasureController(tb,this)); //  add a listener to each block, monitor whether it  is clicked
			tb[i].setBounds((i/3)*130,(i%3)*123 , 123, 120);
	//		JPanel panel=new JPanel(new GridLayout(x,y));
	//		panel.add(tb[i]);

		   mf.mainframe.add(tb[i]);
		//	mf.mainframe.add(panel);
		}

	   mf.gameover=false;
	}

}
